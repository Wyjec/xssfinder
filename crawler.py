from bs4 import BeautifulSoup
import urllib2
import re
import sys


urls=[]
findings = {"form": "action", "a": "href" }

def GetUrls(url, count, session = None):
  global chosenUrl
  chosenUrl=url

  urls.append(chosenUrl)
  for singleLink in urls:
    if len(urls) >= count:
      break
    else:
      Crawl(singleLink, count, session)

  return urls
  

def Crawl(url, count, session = None):
  html=""
  url="http://"+url
  if session:
    html = session.get(url)
    
  else:
    try:
      html = urllib2.urlopen(url)
      
    except urllib2.URLError, e:
      print e
      return  
  soup = BeautifulSoup(html, "lxml")

  
  for key, value in findings.items():
    for link in soup.findAll(key):  #searching by the predefined tags
      if len(urls) >= count:
        break

      try:
        innerUrl=link.get(value)
        if innerUrl.startswith('/'):
          if chosenUrl.split('/')[0] + innerUrl not in urls:
            urls.append(chosenUrl.split('/')[0] + innerUrl)

        elif innerUrl.startswith(chosenUrl):
          if innerUrl not in urls:
            urls.append(innerUrl)

      except:
        pass

