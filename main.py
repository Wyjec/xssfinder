#!/usr/bin/env python
import argparse
import re
from crawler import GetUrls
from xssChecker import XssFinder
from login import *

# TODO: Definitely it should be wrapped into clasess...

def info():
  print """
  Application scans the pages in terms of the XSS Vulnerabilities.
  Additional, user can create an entire site map, which will be scanned later on.
  \033[91mCAUTION! It is illegal to scan any page without prior mutual consent.
  Author assume no liability and are not responsible for any misuse.\033[0m
  
  Kamil Medzikowski 
  """

def FindWithMultipleURLS(listOfUrls, chosenMethod):
  for url in listOfUrls:
    print url
    XssFinder(url, chosenMethod)


def SinglePageScanner(url, chosenMethod, session = None):
  XssFinder(url, chosenMethod, session)

def main():
  info()
  parser = argparse.ArgumentParser()
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument("-s", "--single", action="store_true", help="test particular site")
  group.add_argument("-c", "--crawler", action="store_true", help="crawl and entire site, and test every single page")
  group.add_argument("-l", "--login", action="store_true", help="test particular site after Signing in")
  parser.add_argument("url", help="Page URL")
  parser.add_argument("method", choices=['get', 'post'], help="Choose the method", type=lambda s : s.lower())

  args = parser.parse_args()
 
  if re.compile("^(http|https):").search(args.url):
    raise ValueError("Invalid URL. Url should not begin with http[s] prefix")
  if args.crawler:
    print "Max urls to retrieve?"
    howManyUrls=input()+1
    allUrls = GetUrls(args.url, howManyUrls)
    if not allUrls:
      raise BaseException("No URLs have been found!")
    else:
      FindWithMultipleURLS(allUrls, args.method)
  elif args.single:
    SinglePageScanner(args.url, args.method)
  elif args.login:
    s = GetUserInput()
    SinglePageScanner(args.url, args.method, s)

if __name__ == "__main__":
  main()