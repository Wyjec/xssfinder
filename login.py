import requests
import re
import sys
from bs4 import BeautifulSoup
import urllib, urllib2

__all__= ['GetUserInput']


payload={}

#Log to a chosen page
def Session(loginPage, loginAuthURL, payload):
  s = requests.session()
  s.get(loginPage)
  s.post(loginAuthURL, data=payload)
  return s
  
  
#Find all input names that is related to credentials
def FindCredentialsInput(fields):
  searchedValues=['login', 'pass', 'user']
  keys=[]
  for key, value in fields.items():
    if any(k in key for k in searchedValues):
      if not fields[key]:
        keys.append(key)
  return keys
  
# Gather all input fields that is connected to autorisation request  
def ExtractFormFields(soup):
    
    fields = {}
    for input in soup.findAll('input'):
      if 'type' not in str(input):
   	if 'user' in str(input) or 'login' in str(input):
    	  value = input['value']
    	  fields[input['name']] = value
        continue
      if input['type'] in ('submit') and not input.has_attr('name'):
        continue
        
      if input['type'] in ('text', 'hidden', 'password', 'submit', 'image'):
        value = ''
        if input.has_attr('value'):
          value = input['value']
        fields[input['name']] = value
        continue
        
      if input['type'] in ('checkbox', 'radio'):
        value = ''
        if input.has_attr('checked'):
          if input.has_attr('value'):
            value = input['value']
          else:
            value = 'on'
          if (input['name']) in fields and value:
            fields[input['name']] = value
            
          if not (input['name']) in fields:
            fields[input['name']] = value
            
          continue
        
    
    return fields

# Simple user input method
def GetUserInput():
  global payload
  
  loginSite=raw_input("URL to login site: ")
  while not re.match("^(http|https)://", loginSite):
    loginSite=raw_input("You must provide a valid URL address: ")
    
  html = urllib2.urlopen(loginSite)
  soup = BeautifulSoup(html, "lxml")
  
  payload = ExtractFormFields(soup)
  inputs = FindCredentialsInput(payload)
  
  for singleInput in inputs:
    payload[singleInput] = raw_input("Enter the %s: " % singleInput)
  
  requestSite=raw_input("URL where authorisation request goes to: ")
  while not re.match("^(http|https)://", requestSite):
    requestSite=raw_input("You must provide a valid URL address: ")
  
  #Make Request
  return Session(loginSite, requestSite, payload)